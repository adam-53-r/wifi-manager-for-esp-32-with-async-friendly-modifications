#ifndef WIFI_MANAGER_H
#define WIFI_MANAGER_H

#include <ESPAsyncWebServer.h>

#include "Configuration.h"

class WifiManagerClass {
	public:
		WifiManagerClass();

		bool connectToWifi(bool dont_wait = false);

		void startManagementServer();
		void startManagementServer(const bool skip_scan);
		void startManagementServer(const char *ssid, const bool skip_scan = false);

		void check();
        void startNetworkScan();
        bool checkNetworkScan();

		String getHostname();
		String getSSID();
		int8_t getRSSI();
		IPAddress getIP();

		bool isConnected();

	private:
		AsyncWebServer _server;
		Configuration _config;

		bool _connected;

		int _reconnectIntervalCheck;
		int _connectionTimeout;

		String _networks;
		String _hostname;
		String _ssid;

		IPAddress _ip;

		unsigned long _nextReconnectCheck;

		String getAvailableNetworks();

		bool waitForConnection();
		void serveDefaultUI();
		bool acceptsCompressedResponse(AsyncWebServerRequest *request);
};

extern WifiManagerClass WifiManager;

#endif
